from django.shortcuts import render

# Create your views here.
def home(rqst):
    return render(rqst,"home.html")
def about(rqst):
    return render(rqst,"about.html")
def contact(rqst):
    return render(rqst,"contact.html")
def property(rqst):
    return render(rqst,"property.html")
def property_details(rqst):
    return render(rqst,"property_details.html")
def reset(rqst):
    return render(rqst,"resetpswrd.html")
def signin(rqst):
    return render(rqst,"sign_in.html")
def signup(rqst):
    return render(rqst,"sign_up.html")